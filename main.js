// import {cubeVerts, cubeIndices, cubeUV} from './cube_data.js';

const {mat2, mat3, mat4, vec2, vec3, vec4} = glMatrix;


var canvasWidth = 500;
var canvasHeight = 500;

var canvas = document.getElementById("canvasGL");
canvas.width = canvasWidth;
canvas.height = canvasHeight;

var gl = canvas.getContext('webgl2', {antialias: false});

// Compile shaders ----------------------------------------------

const getShader = (id, shader_type) => {
	var shader;
	var shaderSource = document.getElementById(id).text;

	if (shader_type == 'vs') {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else if (shader_type == 'fs') {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else {
		console.log('Invalid shader script id');
		return;
	}

	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return;
	}
	return shader;
};

var mainVS = getShader("main-vs", "vs");
var mainFS = getShader("main-fs", "fs");

// Create programs ----------------------------------------------

const getProgram = (vs, fs) => {
	var program = gl.createProgram();
	gl.attachShader(program, vs);
	gl.attachShader(program, fs);
	gl.linkProgram(program);
	return program;
};

var mainProgram = getProgram(mainVS, mainFS);
gl.useProgram(mainProgram);

// Create buffers ----------------------------------------------

// Cube

var cubeVertsBuf 	= gl.createBuffer();
var cubeIndicesBuf 	= gl.createBuffer();
var cubeUVBuf 		= gl.createBuffer();

gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertsBuf);
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVerts), gl.STATIC_DRAW);

gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeIndicesBuf);
gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);

gl.bindBuffer(gl.ARRAY_BUFFER, cubeUVBuf);
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeUV), gl.STATIC_DRAW);

// Plain

var plainVertsBuf 	= gl.createBuffer();
var plainIndicesBuf = gl.createBuffer();
var plainUVBuf 		= gl.createBuffer();

gl.bindBuffer(gl.ARRAY_BUFFER, plainVertsBuf);
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plainVerts), gl.STATIC_DRAW);

gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, plainIndicesBuf);
gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(plainIndices), gl.STATIC_DRAW);

gl.bindBuffer(gl.ARRAY_BUFFER, plainUVBuf);
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(plainUV), gl.STATIC_DRAW);

// Get GLSL variables ----------------------------------------------

var uPmatrix 	= gl.getUniformLocation(mainProgram, 'uPmatrix');
var uMmatrix 	= gl.getUniformLocation(mainProgram, 'uMmatrix');
var uVmatrix 	= gl.getUniformLocation(mainProgram, 'uVmatrix');

var uCubeTex 	= gl.getUniformLocation(mainProgram, 'uCubeTex');
var uRenderTex 	= gl.getUniformLocation(mainProgram, 'uRenderTex');
var uLutTex 	= gl.getUniformLocation(mainProgram, 'uLutTex');

var uIsLut 		= gl.getUniformLocation(mainProgram, 'uIsLut');

var aPosition 	= gl.getAttribLocation(mainProgram, 'aPosition');
var aUV 		= gl.getAttribLocation(mainProgram, 'aUV');

gl.enableVertexAttribArray(aPosition);
gl.enableVertexAttribArray(aUV);

// Create matrices ----------------------------------------------

var PROJMATRIX 	= mat4.create();
var MODELMATRIX = mat4.create();
var VIEWMATRIX 	= mat4.create();

// Cube texture -------------------------------------------------

var cubeImg = new Image();
var cubeTextureReader = new FileReader();

var cubeTexture = null;

cubeImg.onload = function(e) {
	cubeTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, cubeTexture);

	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, cubeImg.width, cubeImg.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, cubeImg);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
};
cubeImg.src = "default.jpg";

cubeTextureReader.onload = function (e) {
    cubeImg.src = e.target.result;
}

const loadCustomTexture = (elm) => {
	cubeTexture = null;
 	cubeTextureReader.readAsDataURL(elm.files[0]);
}

// LUT texture ---------------------------------------------------

var lutImg  = new Image();
var lutReader = new FileReader();

lutImg.texture = null;

lutImg.onload = function(e) {
	texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);

	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, lutImg.width, lutImg.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, lutImg);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

	lutImg.texture = texture;
};

lutReader.onload = function (e) {
    lutImg.src = e.target.result;
}

const setLut = (elm) => {
	var img_url = elm.getAttribute("data-image");
	console.log(img_url);

	lutImg.texture = null;
	lutImg.src = img_url;
}

const loadCustomLut = (elm) => {
	lutImg.texture = null;
    lutReader.readAsDataURL(elm.files[0]);
}

// Frame buffer ----------------------------------------------

var targetTexture = gl.createTexture();

gl.activeTexture(gl.TEXTURE0 + 0);
gl.bindTexture(gl.TEXTURE_2D, targetTexture);

gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, canvasWidth, canvasHeight, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

const fb = gl.createFramebuffer();

gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, targetTexture, 0);

// ----------------------------------------------------------------------------

const drawCube = () => {
	mat4.perspective(PROJMATRIX, 45, canvasWidth / canvasHeight, 1, 100);
	mat4.identity(MODELMATRIX);
	mat4.identity(VIEWMATRIX);

	mat4.rotateY(VIEWMATRIX, VIEWMATRIX, -0.5);
	mat4.rotateX(VIEWMATRIX, VIEWMATRIX, 0.5);
	mat4.translate(VIEWMATRIX, VIEWMATRIX, [-3.0, -2.5, -5.0]);

	gl.uniformMatrix4fv(uPmatrix, false, PROJMATRIX);
	gl.uniformMatrix4fv(uMmatrix, false, MODELMATRIX);
	gl.uniformMatrix4fv(uVmatrix, false, VIEWMATRIX);

	gl.uniform1i(uCubeTex, 0);
	gl.uniform1i(uIsLut, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertsBuf);
	gl.vertexAttribPointer(aPosition, 3, gl.FLOAT, false, 4 * 3, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeUVBuf);
	gl.vertexAttribPointer(aUV, 2, gl.FLOAT, false, 4 * 2, 0);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeIndicesBuf);
	gl.drawElements(gl.TRIANGLES, cubeIndices.length, gl.UNSIGNED_SHORT, 0);
}

const drawPlain = () => {
	mat4.identity(PROJMATRIX);
	mat4.identity(MODELMATRIX);
	mat4.identity(VIEWMATRIX);

	mat4.perspective(PROJMATRIX, 45, canvasWidth / canvasHeight, 1, 100);
	mat4.translate(VIEWMATRIX, VIEWMATRIX, [0, 0, -2.5]);

	gl.uniformMatrix4fv(uPmatrix, false, PROJMATRIX);
	gl.uniformMatrix4fv(uMmatrix, false, MODELMATRIX);
	gl.uniformMatrix4fv(uVmatrix, false, VIEWMATRIX);

	gl.uniform1i(uRenderTex, 0);
	gl.uniform1i(uLutTex, 1);

	if (!lutImg.texture) {
		gl.uniform1i(uIsLut, 0);
	}
	else {
		gl.uniform1i(uIsLut, 1);
		gl.activeTexture(gl.TEXTURE0 + 1);
		gl.bindTexture(gl.TEXTURE_2D, lutImg.texture);
		gl.activeTexture(gl.TEXTURE0 + 0);
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, plainVertsBuf);
	gl.vertexAttribPointer(aPosition, 3, gl.FLOAT, false, 4 * 3, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, plainUVBuf);
	gl.vertexAttribPointer(aUV, 2, gl.FLOAT, false, 4 * 2, 0);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, plainIndicesBuf);
	gl.drawElements(gl.TRIANGLES, plainIndices.length, gl.UNSIGNED_SHORT, 0);
}

const drawScene = () => {
	gl.enable(gl.CULL_FACE);
	gl.enable(gl.DEPTH_TEST);

	{
		gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

		gl.bindTexture(gl.TEXTURE_2D, cubeTexture);

		gl.viewport(0, 0, canvasWidth, canvasHeight);

		gl.clearColor(0.3, 0.3, 0.3, 1);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		drawCube();
    }

    {
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      gl.bindTexture(gl.TEXTURE_2D, targetTexture);

      gl.viewport(0, 0, canvasWidth, canvasHeight);

      gl.clearColor(0.7, 0.7, 0.7, 1);
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      drawPlain();
    }

    window.requestAnimationFrame(drawScene);
}

drawScene();

